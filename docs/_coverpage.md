![logo](v200/kotime.png)

<small>v2.0.9@Huoyo</small>

> koTime是一个springboot方法调用链路追踪和运行时长统计工具

- web展示方法调用链路，瓶颈可视化追踪
- 实时监听方法，统计运行时长


[Gitee](https://gitee.com/huoyo/ko-time)
[文档教程](v204/introduce)
